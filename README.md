# Project Card-Game

This is a Texas Hold'em poker game with support for playing against other players and bots.

# Framework and libraries used
* [Qt 5.12.2](https://www.qt.io/)
* [Catch2](https://github.com/catchorg/Catch2)

# Demo video
* https://youtu.be/Hw9W8oyhyn8

# Licenses for assets used in the game
The dissolve noise texture is from Wikimedia, and is made available under the Creative Commons CC0 1.0 Universal Public Domain Dedication. https://creativecommons.org/publicdomain/zero/1.0/deed.en

The card images are taken from Wikipedia and are liscensed under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

26777__junggle__btn402.mp3 is licensed under the Attribution License.
Made by junggle on freesound.org

366102__original-sound__confirmation-upward.wav is licensed under the Attribution License.
Made by original_sound on freesound.org

246420__waveplaysfx__media-game-sound-sfx-short-generic-menu.wav licensed under the Creative Commons 0 License.

256455__deleted-user-4772965__mouse-click.wav is licensed under the Creative Commons 0 License.

145434__soughtaftersounds__old-music-box-1.mp3 is licensed under the Attribution License.
Made by Soughtaftersounds on freesound.org

277033__headphaze__ui-completed-status-alert-notification-sfx001.wav is licensed under the Attribution License.
Made by Headphaze on freesound.org

240776__f4ngy__card-flip.wav is licensed under the Attribution License.
Made by f4ngy on freesound.org

171697__nenadsimic__menu-selection-click.wav is licensed under the Creative Commons 0 License.

Coffee Tin Font is licensed as Freeware (https://www.fontspace.com/coffee-tin-font-f5796)

## Developers

- [Radomir Vujosevic, 380/2020](https://gitlab.com/RadomirVujosevic)
- [Nikola Nikolic, 35/2015](https://gitlab.com/nnikolicc)
- [Luka Colic, 31/2018](https://gitlab.com/LCEnzo)
- [Lazar Cvejic, 249/2018](https://gitlab.com/lazarcv)
