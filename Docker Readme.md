

# Uputstva

Za docker-izovanje i pokretanje programa u kontejneru potrebno je pokrenuti sledece komande:

> docker-compose build
> xhost +local:docker
> sudo docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix/: azrs-tracking-card-game
