# Igranje jedne partije

**Kratak opis**: Program učitava podešavanja igre. Igrač bira željeni broj igrača i opciono zove druge korisnike da se priključe partiji preko mreže. Preostala mesta popunjavaju botovi. Igrač onda započinja i igra partiju.

**Akteri**: Lokalni igrač, botovi, korisnici sa mreže

**Preduslovi**: Aplikacija je pokrenuta i Igrač je ušao u meni za partiju.

**Postuslovi**: Partija je uspešno završena.

**Osnovni tok**:
1. Aplikacija dohvata podešavanja aplikacije.
2. Lokalni igrač bira broj igrača.
3. Lokalni igrač poziva druge korisnike.
4. Opciono lokalni korisnik može da podesi ponovo broj igrača da ima željeni broj botova u partiji.
5. Lokalni korisnik započinje igru.
6. Sve dok bar dva igrača imaju više od 0 novca, ponavljaju se sledeći koraci:
	* 6.1. 
    * 6.2. 
		* 6.2.1. 
		* 6.2.2. 
	* 6.3. 

**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svi igrači moraju biti povezani na istu mrežu.

**Dodatne informacije**: /


