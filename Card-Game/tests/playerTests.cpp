#include "catch.hpp"
#include "client/player.cpp"

TEST_CASE( "Igrac toString()", "[Player]" ) {
    SECTION("No cards") {
        // Arrange
        Player p1, p2;
        QString s1, s2;

        // Act
        p1 = Player();
        p2 = Player(1, 10000, "Player 2", Player::AI);

        s1 = QString("Player 0 has 0 money");
        s2 = QString("Player 1 has 10000 money");

        // Assert
        CHECK_FALSE( s1.compare(p1.toString()) == 0 );
        REQUIRE( s2.compare(p2.toString()) == 0 );
    }
    /*
     * Todo when Player has method addCards
    SECTION("With 2 cards") {
        Player p;
        QString s;
        Card c1, c2;

        p = Player(420, 1337, " - zika - ", Player::Human);

        c1 = Card(CardSymbol::Clubs, 5);
        c2 = Card(CardSymbol::Clubs, 5);


        s = QString("Player 1 has 10000 money and cards %1 and %2");

        REQUIRE( s.compare(p.toString()) == 0 );
    }
    */
}
