QT += core network gui widgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    client/card.cpp \
	client/cardhand.cpp \
	client/cardnumber.cpp \
	client/cardsymbol.cpp \
	client/clientcontroller.cpp \
	client/gameclient.cpp \
	client/player.cpp \
    client/aiaction.cpp \
    main.cpp \
	server/gameserver.cpp \
	server/servercontroller.cpp \
	server/serverworker.cpp \
    ui/layouts/betinput.cpp \
    ui/layouts/cardlayout.cpp \
    ui/layouts/playerlayout.cpp \
    ui/layouts/tablelayout.cpp \
    ui/soundmanager.cpp \
    ui/widget.cpp

HEADERS += \
    client/aiaction.h \
    client/card.h \
    client/cardhand.h \
    client/cardnumber.h \
	client/cardsymbol.hpp \
	client/clientcontroller.h \
	client/gameclient.h \
    client/player.h \
	server/gameserver.h \
	server/servercontroller.h \
	server/serverworker.h \
    ui/layouts/betinput.h \
    ui/layouts/cardlayout.h \
    ui/layouts/playerlayout.h \
    ui/layouts/tablelayout.h \
    ui/soundmanager.h \
    ui/widget.h

FORMS += \
    client/clientcontroller.ui \
	server/servercontroller.ui \
    ui/layouts/betinput.ui \
    ui/widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    512x512_Dissolve_Noise_Texture one and half percent opacity.png \
    font/CoffeetinInitials-YXJ2.ttf \
    images/512x512_Dissolve_Noise_Texture 20% opacity.png \
    images/512x512_Dissolve_Noise_Texture 3,7% opacity.png \
    images/512x512_Dissolve_Noise_Texture 5% opacity.png \
    images/512x512_Dissolve_Noise_Texture 7,5% opacity.png \
    images/512x512_Dissolve_Noise_Texture one and half percent opacity.png \
	images/512x512_Dissolve_Noise_Texture one and half percent opacity.png \
	512x512_Dissolve_Noise_Texture 3,7% opacity.png \
	images/512x512_Dissolve_Noise_Texture 3,7% opacity.png \
	512x512_Dissolve_Noise_Texture 5% opacity.png \
	images/512x512_Dissolve_Noise_Texture 5% opacity.png \
	512x512_Dissolve_Noise_Texture 7,5% opacity.png \
	images/512x512_Dissolve_Noise_Texture 7,5% opacity.png \
	512x512_Dissolve_Noise_Texture 20% opacity.png \
	images/512x512_Dissolve_Noise_Texture 20% opacity.png \
	512x512_Dissolve_Noise_Texture \
	images/512x512_Dissolve_Noise_Texture \
    images/512x512_Dissolve_Noise_Texture.png \
    images/cards/back.png \
    images/cards/club-1.png \
    images/cards/club-10.png \
    images/cards/club-12.png \
    images/cards/club-13.png \
    images/cards/club-14.png \
    images/cards/club-2.png \
    images/cards/club-3.png \
    images/cards/club-4.png \
    images/cards/club-5.png \
    images/cards/club-6.png \
    images/cards/club-7.png \
    images/cards/club-8.png \
    images/cards/club-9.png \
    images/cards/diamond-1.png \
    images/cards/diamond-10.png \
    images/cards/diamond-12.png \
    images/cards/diamond-13.png \
    images/cards/diamond-14.png \
    images/cards/diamond-2.png \
    images/cards/diamond-3.png \
    images/cards/diamond-4.png \
    images/cards/diamond-5.png \
    images/cards/diamond-6.png \
    images/cards/diamond-7.png \
    images/cards/diamond-8.png \
    images/cards/diamond-9.png \
    images/cards/heart-1.png \
    images/cards/heart-10.png \
    images/cards/heart-12.png \
    images/cards/heart-13.png \
    images/cards/heart-14.png \
    images/cards/heart-2.png \
    images/cards/heart-3.png \
    images/cards/heart-4.png \
    images/cards/heart-5.png \
    images/cards/heart-6.png \
    images/cards/heart-7.png \
    images/cards/heart-8.png \
    images/cards/heart-9.png \
    images/cards/invisible.png \
    images/cards/joker-1.png \
    images/cards/joker-2.png \
    images/cards/spade-1.png \
    images/cards/spade-10.png \
    images/cards/spade-12.png \
    images/cards/spade-13.png \
    images/cards/spade-14.png \
    images/cards/spade-2.png \
    images/cards/spade-3.png \
    images/cards/spade-4.png \
    images/cards/spade-5.png \
    images/cards/spade-6.png \
    images/cards/spade-7.png \
    images/cards/spade-8.png \
    images/cards/spade-9.png \
    sound/145434__soughtaftersounds__old-music-box-1.mp3 \
    sound/145434__soughtaftersounds__old-music-box-1.wav \
    sound/171697__nenadsimic__menu-selection-click.wav \
    sound/240776__f4ngy__card-flip.wav \
    sound/246420__waveplaysfx__media-game-sound-sfx-short-generic-menu.wav \
    sound/246420__waveplaysfx__media-game-sound-sfx-short-generic-menu1.wav \
    sound/256455__deleted-user-4772965__mouse-click.wav \
    sound/26777__junggle__btn402.mp3 \
    sound/26777__junggle__btn402.wav \
    sound/277033__headphaze__ui-completed-status-alert-notification-sfx001.wav \
    sound/366102__original-sound__confirmation-upward.wav \
    ui/widget.qss \
    widget.qss

RESOURCES += \
    resources.qrc \
    resources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SUBDIRS += \
    test.pro
