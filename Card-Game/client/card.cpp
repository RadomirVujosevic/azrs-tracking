#include "card.h"

QString Card::toString(){
    return QStringLiteral("%2 of %1").arg(symbol.toString(), number.toString());
}
Card::Card(CardSymbol symbol, CardNumber number)
    :symbol(symbol),
     number(number){}

Card::Card(CardSymbol symbol, int number)
    :symbol(symbol),
     number(CardNumber::intToNumber(number)){}

Card::Card(QString symbol, QString number)
    :symbol(CardSymbol::stringToSymbol(symbol)),
     number(CardNumber::stringToNumber(number)){}

Card::Card(QString symbol, int number)
    :symbol(CardSymbol::stringToSymbol(symbol)),
     number(CardNumber::intToNumber(number)){}

CardNumber Card::getNumber() const{
    return number;
}

CardSymbol Card::getSymbol(){
    return symbol;
}

bool Card::operator==(Card c){
    return number == c.number && symbol == c.symbol;
}

bool Card::operator!=(Card c){
    return !(*this == c);
}

Card Card::operator+(int n){
    return Card{symbol, number + n};
}

bool Card::operator<(const Card c) const{
    return number < c.number;
}
