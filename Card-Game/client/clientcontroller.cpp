#include "clientcontroller.h"
#include "ui_clientcontroller.h"
#include "client/gameclient.h"
#include <QStandardItemModel>
#include <QInputDialog>
#include <QMessageBox>
#include <QHostAddress>
ClientController::ClientController(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ClientController)
    , m_gameClient(new GameClient(this))
    , m_gameModel(new QStandardItemModel(this))
{
    ui->setupUi(this);
    m_gameModel->insertColumn(0);
    ui->gameView->setModel(m_gameModel);

    connect(m_gameClient, &GameClient::connected, this, &ClientController::connectedToServer);
    connect(m_gameClient, &GameClient::loggedIn, this, &ClientController::loggedIn);
    connect(m_gameClient, &GameClient::loginError, this, &ClientController::loginFailed);
    connect(m_gameClient, &GameClient::disconnected, this, &ClientController::disconnectedFromServer);
    connect(m_gameClient, &GameClient::error, this, &ClientController::error);
    connect(ui->connectButton, &QPushButton::clicked, this, &ClientController::attemptConnection);
    connect(ui->playButton, &QPushButton::clicked, this, &ClientController::requestBet);
    connect(m_gameClient, &GameClient::betReceived, this, &ClientController::betReceived);
    connect(m_gameClient, &GameClient::gameStarted, this, &ClientController::gameStarted);
    connect(m_gameClient, &GameClient::playerAdded, this, &ClientController::playerAdded);
    connect(m_gameClient, &GameClient::playerRemoved, this, &ClientController::playerRemoved);
    connect(m_gameClient, &GameClient::foldReceived, this, &ClientController::foldReceived);
    connect(m_gameClient, &GameClient::roundStarted, this, &ClientController::roundStarted);
    connect(m_gameClient, &GameClient::cardsReceived, this, &ClientController::cardsReceived);
    connect(m_gameClient, &GameClient::flopReceived, this, &ClientController::flopReceived);
    connect(m_gameClient, &GameClient::turnReceived, this, &ClientController::turnReceived);
    connect(m_gameClient, &GameClient::riverReceived, this, &ClientController::riverReceived);
    connect(m_gameClient, &GameClient::waitingForInput, this, &ClientController::waitingForInput);
    connect(m_gameClient, &GameClient::playerToMove, this, &ClientController::playerToMove);
    connect(m_gameClient, &GameClient::changeMoney, this, &ClientController::changeMoney);
    connect(m_gameClient, &GameClient::gameEnded, this, &ClientController::gameEnded);
    connect(m_gameClient, &GameClient::showCards, this, &ClientController::showCards);
    connect(m_gameClient, &GameClient::playerWon, this, &ClientController::playerWon);

    gui = new Widget();
    gui->show();

    connect(this, &ClientController::gameStartedSignal, gui, &Widget::gameStarted);
    connect(this, &ClientController::playerBetChangedSignal, gui, &Widget::playerBetChanged);
    connect(this, &ClientController::potChangedSignal, gui, &Widget::potChanged);
    connect(this, &ClientController::tableCardsChangedSignal, gui, &Widget::tableCardsChanged);
    connect(this, &ClientController::playerMoneyChangedSignal, gui, &Widget::playerMoneyChanged);
    connect(this, &ClientController::playerCardsChangedSignal, gui, &Widget::playerCardsChanged);
    connect(this, &ClientController::playerLostSignal, gui, &Widget::playerLost);
    connect(this, &ClientController::playerFoldedSignal, gui, &Widget::playerFolded);
    connect(this, &ClientController::roundStartedSignal, gui, &Widget::roundStarted);
    connect(this, &ClientController::gameEndedSignal, gui, &Widget::gameEnded);
    connect(this, &ClientController::requestBetSignal, gui, &Widget::requestBetSlot);
    connect(this, &ClientController::playerWonSignal, gui, &Widget::playerWon);
    connect(gui, &Widget::returnBetToClient, this, &ClientController::receiveBetFromLocalPlayer);
    connect(gui, &Widget::attemptConnection, this, &ClientController::attemptConnection);
    connect(gui, &Widget::attemptConnectionToAddress, this, &ClientController::attemptConnectionToAddress);
    connect(gui, &Widget::playerQuitSignal, this, &ClientController::disconnectFromServer);
    connect(this, &ClientController::playerLostSignal, gui, &Widget::playerLost);
}

ClientController::~ClientController()
{
    delete ui;
}

void ClientController::attemptConnectionToAddress(QString hostAddress)
{
    if (hostAddress.isEmpty())
        return;
    ui->connectButton->setEnabled(false);
    m_gameClient->connectToServer(QHostAddress(hostAddress), 1967);
}


void ClientController::attemptConnection()
{
    const QString hostAddress = QInputDialog::getText(
        this
        , tr("Chose Server")
        , tr("Server Address")
        , QLineEdit::Normal
        , QStringLiteral("127.0.0.1")
    );
    attemptConnectionToAddress(hostAddress);
}

void ClientController::connectedToServer()
{
    const QString newUsername = gui->getName();
    if (newUsername.isEmpty()){
        return m_gameClient->disconnectFromHost();
    }
    attemptLogin(newUsername);
}

void ClientController::attemptLogin(const QString &userName)
{
    m_gameClient->login(userName);
}

void ClientController::loggedIn(int id)
{
    ui->sendButton->setEnabled(true);
    ui->messageEdit->setEnabled(true);
    ui->gameView->setEnabled(true);
    const int newRow = m_gameModel->rowCount();
    m_gameModel->insertRow(newRow);
    m_gameModel->setData(m_gameModel->index(newRow, 0), tr("Logged in with id %1").arg(id));
    m_gameModel->setData(m_gameModel->index(newRow, 0), Qt::AlignCenter, Qt::TextAlignmentRole);
    m_gameModel->setData(m_gameModel->index(newRow, 0), QBrush(Qt::red), Qt::ForegroundRole);
    ui->gameView->scrollToBottom();
    m_lastUserName.clear();

    myID = id;
}

void ClientController::loginFailed(const QString &reason)
{
    QMessageBox::critical(this, tr("Error"), reason);
}

void ClientController::disconnectFromServer()
{
    qDebug() << "FILE: " << __FILE__ << ", FUNCTION: "
             << __FUNCTION__ << " called";
    m_gameClient->disconnectFromHost();
}

void ClientController::disconnectedFromServer()
{
    //QMessageBox::warning(this, tr("Disconnected"), tr("Connection terminated"));
    ui->sendButton->setEnabled(false);
    ui->messageEdit->setEnabled(false);
    ui->gameView->setEnabled(false);
    ui->connectButton->setEnabled(true);
    m_lastUserName.clear();
}

void ClientController::error(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::ProxyConnectionClosedError:
        return; // handled by disconnectedFromServer
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::critical(this, tr("Error"), tr("The host refused the connection"));
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        QMessageBox::critical(this, tr("Error"), tr("The proxy refused the connection"));
        break;
    case QAbstractSocket::ProxyNotFoundError:
        QMessageBox::critical(this, tr("Error"), tr("Could not find the proxy"));
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::critical(this, tr("Error"), tr("Could not find the server"));
        break;
    case QAbstractSocket::SocketAccessError:
        QMessageBox::critical(this, tr("Error"), tr("You don't have permissions to execute this operation"));
        break;
    case QAbstractSocket::SocketResourceError:
        QMessageBox::critical(this, tr("Error"), tr("Too many connections opened"));
        break;
    case QAbstractSocket::SocketTimeoutError:
        QMessageBox::warning(this, tr("Error"), tr("Operation timed out"));
        return;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        QMessageBox::critical(this, tr("Error"), tr("Proxy timed out"));
        break;
    case QAbstractSocket::NetworkError:
        QMessageBox::critical(this, tr("Error"), tr("Unable to reach the network"));
        break;
    case QAbstractSocket::UnknownSocketError:
        QMessageBox::critical(this, tr("Error"), tr("An unknown error occured"));
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        QMessageBox::critical(this, tr("Error"), tr("Operation not supported"));
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        QMessageBox::critical(this, tr("Error"), tr("Your proxy requires authentication"));
        break;
    case QAbstractSocket::ProxyProtocolError:
        QMessageBox::critical(this, tr("Error"), tr("Proxy comunication failed"));
        break;
    case QAbstractSocket::TemporaryError:
    case QAbstractSocket::OperationError:
        QMessageBox::warning(this, tr("Error"), tr("Operation failed, please try again"));
        return;
    default:
        Q_UNREACHABLE();
    }
    ui->connectButton->setEnabled(true);
    ui->sendButton->setEnabled(false);
    ui->messageEdit->setEnabled(false);
    ui->gameView->setEnabled(false);
    m_lastUserName.clear();
}

void ClientController::requestBet()
{
    m_gameClient->betRequest(ui->messageEdit->text().toInt());
    ui->messageEdit->clear();
}

void ClientController::betReceived(int id, int bet)
{
    const int newRow = m_gameModel->rowCount();
    m_gameModel->insertRow(newRow);
    m_gameModel->setData(m_gameModel->index(newRow, 0), tr("%1 played %2").arg(id).arg(bet));
    m_gameModel->setData(m_gameModel->index(newRow, 0), Qt::AlignCenter, Qt::TextAlignmentRole);
    m_gameModel->setData(m_gameModel->index(newRow, 0), QBrush(Qt::red), Qt::ForegroundRole);
    ui->gameView->scrollToBottom();
    m_lastUserName.clear();

#ifdef DEBUG
    qDebug() << "Player ID:" << id << "Made bet:" << bet << " FILE:" << __FILE__ << "Function:" << __FUNCTION__;
#endif
    players[getPlayerByID(id)].makeBet(bet);
    emit playerBetChangedSignal(id, players[getPlayerByID(id)].getBet());
    emit playerMoneyChangedSignal(id, players[getPlayerByID(id)].getMoney());
    changePot(pot + bet);
}
void ClientController::gameStarted(QVector<Player>& players, int startingMoney)
{
    int newRow = m_gameModel->rowCount();
    m_gameModel->insertRow(newRow);
    m_gameModel->setData(m_gameModel->index(newRow, 0), tr("Game started"));
    m_gameModel->setData(m_gameModel->index(newRow, 0), Qt::AlignCenter, Qt::TextAlignmentRole);
    m_gameModel->setData(m_gameModel->index(newRow, 0), QBrush(Qt::red), Qt::ForegroundRole);
    ui->gameView->scrollToBottom();
    m_lastUserName.clear();

    newRow = m_gameModel->rowCount();
    m_gameModel->insertRow(newRow);
    m_gameModel->setData(m_gameModel->index(newRow, 0), tr("Starting money: %1").arg(startingMoney));
    m_gameModel->setData(m_gameModel->index(newRow, 0), Qt::AlignCenter, Qt::TextAlignmentRole);
    m_gameModel->setData(m_gameModel->index(newRow, 0), QBrush(Qt::red), Qt::ForegroundRole);
    ui->gameView->scrollToBottom();
    m_lastUserName.clear();

    this->players.clear();
    this->startingMoney = startingMoney;
    for(auto &player: players){
        playerAdded(player.getID(),player.getUsername());
    }

    emit gameStartedSignal(players.size(), this->myID, players, startingMoney);
}

int ClientController::getPlayerByID(int ID){
    for(int i = 0; i < players.size(); i++){
        if(players[i].getID() == ID)
            return i;
    }
#ifdef DEBUG
    qDebug() << "Trying to get ID of player that doesn't exist";
#endif
    return -1;
}

void ClientController::changePot(int newValue){
    pot = newValue;
    emit potChangedSignal(newValue);
}

void ClientController::resetBets(){
    for(auto &p : players){
        p.resetBet();
        emit playerBetChangedSignal(p.getID(), 0);
    }
}

void ClientController::playerAdded(int id, QString username)
{
#ifdef DEBUG
    qDebug() << "Added player ID:" << id << "Username:" << username;
#endif
    players.push_back(Player{id, startingMoney, username});
};

void ClientController::playerRemoved(int id)
{
#ifdef DEBUG
    qDebug() << "Removed player ID:" << id;
#endif

    int idx = getPlayerByID(id);
    players.erase(players.begin() + idx);

    emit playerLostSignal(id);
};

void ClientController::resetPlayerCards(){
    for(auto &p : players){
        p.getCards().clear();
        emit playerCardsChangedSignal(p.getID(), p.getCards());
    }
}

void ClientController::foldReceived(int id)
{
#ifdef DEBUG
    qDebug() << "Player" << id << "folded";
#endif
    emit playerFoldedSignal(id);
};

void ClientController::roundStarted()
{
#ifdef DEBUG
    qDebug() << "New round started";
#endif
    changePot(0);

    communalCards.clear();
    emit tableCardsChangedSignal(communalCards);

    resetPlayerCards();
    resetBets();

    emit roundStartedSignal();
};

void ClientController::cardsReceived(QString symbol1, int number1, QString symbol2, int number2)
{
    Card c1(symbol1, number1);
    Card c2(symbol2, number2);
#ifdef DEBUG
    qDebug() << "Received Cards:";
    qDebug() << c1.toString();
    qDebug() << c2.toString();
#endif
	int index = getPlayerByID(myID);
	if (index == -1) {
		qDebug() << "File:" << __FILE__ << "F():" << __FUNCTION__ << "\nmyID not found in players\n";
		return;
	}

    players[getPlayerByID(myID)].getCards().push_back(c1);
    players[getPlayerByID(myID)].getCards().push_back(c2);

    qDebug() << players[getPlayerByID(myID)].getCards().size();
    emit playerCardsChangedSignal(myID, players[getPlayerByID(myID)].getCards());
};

void ClientController::flopReceived(QString symbol1, int number1, QString symbol2, int number2, QString symbol3, int number3)
{
    Card c1(symbol1, number1);
    Card c2(symbol2, number2);
    Card c3(symbol3, number3);
#ifdef DEBUG
    qDebug() << "Received Flop:";
    qDebug() << c1.toString();
    qDebug() << c2.toString();
    qDebug() << c3.toString();
#endif
    communalCards.push_back(c1);
    communalCards.push_back(c2);
    communalCards.push_back(c3);

    emit tableCardsChangedSignal(communalCards);
};

void ClientController::turnReceived(QString symbol, int number)
{
    Card c1(symbol, number);
#ifdef DEBUG
    qDebug() << "Received Turn:";
    qDebug() << c1.toString();
#endif
    communalCards.push_back(c1);

    emit tableCardsChangedSignal(communalCards);
};

void ClientController::riverReceived(QString symbol, int number)
{
    Card c1(symbol, number);
#ifdef DEBUG
    qDebug() << "Received River:";
    qDebug() << c1.toString();
#endif
    communalCards.push_back(c1);

    emit tableCardsChangedSignal(communalCards);
};

void ClientController::waitingForInput(int id, int bet)
{
#ifdef DEBUG
    qDebug() << "Server is expecting a bet:" << bet << "From Player ID:" << id;
#endif
    if(id == myID) {
        int callAmount = bet;
        emit requestBetSignal(callAmount);
    }
};

void ClientController::playerToMove(int id)
{
#ifdef DEBUG
    qDebug() << "Player to move has ID:" << id;
#endif
    playerToMoveID = id;
};

void ClientController::changeMoney(int id, int money)
{
#ifdef DEBUG
    qDebug() << "Player ID:" << id << "Received money:" << money;
#endif
    players[getPlayerByID(id)].changeMoney(money);
    emit playerMoneyChangedSignal(id, players[getPlayerByID(id)].getMoney());
};

void ClientController::gameEnded()
{
#ifdef DEBUG
    qDebug() << "Game ended";
#endif
    players.clear();
    emit gameEndedSignal();
};

void ClientController::receiveBetFromLocalPlayer(int bet) {
    qDebug() << "My ID: " << myID << ", bet received from gui: " << bet;
    m_gameClient->betRequest(bet);
}

#ifdef DEBUG
void ClientController::minimizeGUI() {
    gui->showMinimized();
}
#endif

void ClientController::showCards(int id, QString symbol1, int number1, QString symbol2, int number2)
{
    Card c1(symbol1, number1);
    Card c2(symbol2, number2);

#ifdef DEBUG
    qDebug() << "Player ID:" << id << "has cards:";
    qDebug() << c1.toString();
    qDebug() << c2.toString();
#endif

    std::vector<Card> cards = {c1, c2};
    emit playerCardsChangedSignal(id, cards);
};


void ClientController::playerWon(int id)
{
#ifdef DEBUG
    qDebug() << "Winner is Player ID:" << id;
#endif
    emit playerWonSignal(id);
};
