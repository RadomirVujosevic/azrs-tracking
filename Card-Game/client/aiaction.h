#ifndef __AI_ACTION__
#define __AI_ACTION__

#include <vector>
#include "card.h"
#include <QString>
#include <map>

class AIAction {
public:
  enum handValue {
    HighCard, Pair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush, RoyalFlush
  };

  AIAction(std::vector<Card> &handCards, std::vector<Card> &commulativeCards);

  int calculateMove();

  handValue evaluateCombination(std::vector<Card> &combination);

  std::map<Card, int> formCardMap(std::vector<Card> &combination);

private:
  std::vector<Card> handCards;
  std::vector<Card> commulativeCards;
  std::vector<Card> combination;
  std::map<handValue, float> probability;
  std::map<handValue, float> comulative;
  void setProbability();
  void setComulative();
  void removeImpossibleCombinations();
  bool isRoyalFlush(std::vector<Card> &combination);
  bool isStraightFlush(std::vector<Card> &combination);
  bool isFourOfAKind(std::vector<Card> &combination, std::map<Card, int> cardCountMap);
  bool isFullHouse(std::vector<Card> &combination, std::map<Card, int> cardCountMap);
  bool isFlush(std::vector<Card> &combination);
  bool isStraight(std::vector<Card> &combination);
  bool isThreeOfAKind(std::vector<Card> &combination, std::map<Card, int> cardCountMap);
  bool isTwoPair(std::vector<Card> &combination, std::map<Card, int> cardCountMap);
  bool isPair(std::vector<Card> &combination, std::map<Card, int> cardCountMap);
  bool isHighCard(std::vector<Card> &combination);

};

#endif
