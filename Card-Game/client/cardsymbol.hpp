#ifndef CARDSYMBOL_HPP
#define CARDSYMBOL_HPP

#pragma once

#include <QString>
#include <QtDebug>

class CardSymbol
{
public:
    enum symbols{
        Clubs, Spades, Hearts, Diamonds
    };
    CardSymbol(symbols s):symbol(s){}

    static QString symbolToString(symbols symbol);
    static QString symbolToString(CardSymbol symbol);
    static CardSymbol stringToSymbol(QString symbol);

    QString toString();

    bool operator ==(CardSymbol cs);
    bool operator !=(CardSymbol cs);



private:
    symbols symbol;

};

#endif // CARDSYMBOL_HPP
