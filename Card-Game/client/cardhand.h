#ifndef CARDHAND_H
#define CARDHAND_H

#include "player.h"
#include "card.h"

class CardHand
{
public:
    CardHand();

    enum Hands{
        None, HighCard, Pair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush, RoyalFlush
    };

    std::vector<Card> allCards;
    std::vector<Card> strongestHand;

    enum Hands bestHandEvaluation;

    static QString HandsToString(Hands hand);

    CardHand(std::vector<Card> &communalCards, std::vector<Card> &playerCards);

    QString toString();

    bool operator <(CardHand &rhs);

private:
    std::vector<Card> currentCombination;
    std::map<Card, int> cardCountMap;
    std::map<Card, int> rCardCountMap;

    void evaluateHand(int offset, int k);
    Hands evaluateCombination(std::vector<Card> &combination);

    //pomocne funkcije za evaluateHand
    bool isRoyalFlush(std::vector<Card> &combination);
    bool isStraightFlush(std::vector<Card> &combination);
    bool isFourOfAKind(std::vector<Card> &combination);
    bool isFullHouse(std::vector<Card> &combination);
    bool isFlush(std::vector<Card> &combination);
    bool isStraight(std::vector<Card> &combination);
    bool isThreeOfAKind(std::vector<Card> &combination);
    bool isTwoPair(std::vector<Card> &combination);
    bool isPair(std::vector<Card> &combination);
    bool isHighCard(std::vector<Card> &combination);

    std::map<Card, int> formCardMap(std::vector<Card> &combination);



    //Poredi rCards vektor i trenutni vektor strongestHand i vraca true ako rCards ima jacu ruku
    bool compareEqualEvals(std::vector<Card> &rCards);

    //Pomocne funkcije za compareEqualEvals
    bool compareHighCard(std::vector<Card> &rCards);
    bool comparePair(std::vector<Card> &rCards);
    bool compareTwoPair(std::vector<Card> &rCards);
    bool compareThreeOfAKind(std::vector<Card> &rCards);
    bool compareFourOfAKind(std::vector<Card> &rCards);
    bool compareStraight(std::vector<Card> &rCards);
    bool compareFlush(std::vector<Card> &rCards);
    bool compareFullHouse(std::vector<Card> &rCards);
    bool compareStraightFlush(std::vector<Card> &rCards);


};

#endif // CARDHAND_H
