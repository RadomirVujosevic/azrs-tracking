#include "player.h"
#include "aiaction.h"
#include <QTimer>
#include <vector>
#include <stdlib.h>

//communalCards su karte na stolu
//cards su karte koje igrac ima
//bet je koliko treba da se opkladi za Call
int Player::getResponse(std::vector<Card> &communalCards, int betToCall, int notFoldedCount){
    //Potrebno je neko malo vreme pauze za "razmisljanje" da bi se izbeglo da 5 botova odigra svoje poteze u isto vreme
    QThread::msleep(100);

    AIAction* action = new AIAction(cards, communalCards);

    int evaluation = action->calculateMove();

    delete action;
    if (evaluation == 0) {
        return 0;
    } else if (evaluation == 2 && !raised) {
        if (betToCall < money) {
            int change = money - betToCall;
            raised = true;
            if (communalCards.size() == 0)
                return (rand() % 10) * change * 0.01 + betToCall;
            else if (rand() % 30 <= rand() % 5)
                return (rand() % 41 + 60) * change * 0.01 + betToCall;
            else return (rand() % 20) * change * 0.01 + betToCall;
        }
    } else {
        return betToCall < money ? betToCall : money;
    }
}


Player::Player(){};
Player::Player(int id, int money, QString username, playerTypes type):
    id(id), money(money), username(username), playerType(type), raised(false)
{}

QString Player::toString(){
    if(cards.size() != 2)
        return QStringLiteral("Player %1 has %2 money").arg(id).arg(money);
    return QStringLiteral("Player %1 has %2 money and cards %3 and %4").arg(id).arg(money).arg(cards[0].toString(), cards[1].toString());
}

void Player::changeMoney(int n){
    money += n;
}

void Player::resetRaised() {
    raised = false;
}

int Player::makeBet(int bet){

    if(bet > money){
        qDebug() << "Bet:" << bet <<  "is higher than remaining money:" << money << "- should be impossible";
    }
    currentBet += bet;
    money -= bet;
    return bet;
}

int Player::getBet() const {
    return currentBet;
}

int Player::getID() const {
    return id;
}

QString Player::getUsername() const {
    return username;
}

Player::playerTypes Player::getPlayerType() const {
    return playerType;
}

std::vector<Card> &Player::getCards(){
    return cards;
}

bool Player::hasFolded(){
    return status == playerStatus::Folded;
}

bool Player::operator==(Player &p){
    return id == p.id;
}

int Player::getMoney() const {
    return money;
}

Player::playerStatus Player::getStatus() const {
    return status;
}

void Player::setStatus(playerStatus ps){
    status = ps;
}

void Player::resetBet(){
    currentBet = 0;
}

void Player::setMoney(int money){
    this->money = money;
}

bool Player::finishedTurn(){
    return status == Folded
                || status == Checked
                || status == AllIn;
}
