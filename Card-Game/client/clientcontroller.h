#ifndef CLIENTCONTROLLER_H
#define CLIENTCONTROLLER_H

#include <QWidget>
#include <QAbstractSocket>
#include "player.h"
#include "cardhand.h"
#include "card.h"
#include "ui/widget.h"

#define DEBUG

class Widget;
class GameClient;
class QStandardItemModel;
namespace Ui { class ClientController; }
class ClientController : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ClientController)
public:
    explicit ClientController(QWidget *parent = nullptr);
    ~ClientController();
    /* kopirano iz clientcontroller.hpp */
    //svi igraci

    enum Stages{
        Preflop, Flop, Turn, River
    };

    // Svi igraci, ClientController nece imati informacije o njihovim kartama
    std::vector<Player> players;

    Stages currentStage;
    //Karte na stolu
    std::vector<Card> communalCards;

    //Lokalni igrac
    int myID;

    //ID igraca koji je na potezu
    int playerToMoveID;
    int pot = 0;
#ifdef DEBUG
    void minimizeGUI();
    Widget* getGUI();
#endif

signals:
    void gameStartedSignal(int playerCount, int localID, const QVector<Player>& players, int initialMoney);
    void playerBetChangedSignal(int id, int money);
    void potChangedSignal(int pot);
    void tableCardsChangedSignal(const std::vector<Card> &cards);
    void playerMoneyChangedSignal(int id, int currentMoney);
    void playerCardsChangedSignal(int id, const std::vector<Card> &cards);
    void playerLostSignal(int id);
    void playerFoldedSignal(int id);
    void roundStartedSignal();
    void gameEndedSignal();
    void requestBetSignal(int callAmount);
    void playerWonSignal(int id);

private:
    //Vraca indeks igraca ko ima ovaj ID ili -1 ako ne postoji
    int getPlayerByID(int ID);
    void resetBets();
    void resetPlayerCards();
    int startingMoney;


    //Pot bi trebalo da se menja iskljucivo preko ove funkcije
    //Svrha je to sto ce ova funkcija emitovati signal ka grafici
    void changePot(int newValue);


    /* kraj kopiranog dela */
    Widget *gui;
    Ui::ClientController *ui;
    GameClient *m_gameClient;
    QStandardItemModel *m_gameModel;
    QString m_lastUserName;

private slots:
    void attemptConnection();
    void attemptConnectionToAddress(QString hostAddress);
    void connectedToServer();
    void attemptLogin(const QString &userName);
    void loggedIn(int id);
    void loginFailed(const QString &reason);
    void disconnectedFromServer();
    void error(QAbstractSocket::SocketError socketError);
    void requestBet();
    void gameStarted(QVector<Player>& players, int startingMoney);
    void playerAdded(int id, QString username);
    void playerRemoved(int id);
    void betReceived(int id, int bet);
    void foldReceived(int id);
    void roundStarted();
    void cardsReceived(QString symbol1, int number1, QString symbol2, int number2);
    void flopReceived(QString symbol1, int number1, QString symbol2, int number2, QString symbol3, int number3);
    void turnReceived(QString symbol, int number);
    void riverReceived(QString symbol, int number);
    void waitingForInput(int id, int bet);
    void playerToMove(int id);
    void changeMoney(int id, int money);
    void gameEnded();
    void receiveBetFromLocalPlayer(int bet);
    void showCards(int id, QString symbol1, int number1, QString symbol2, int number2);
    void playerWon(int id);
    void disconnectFromServer();
};

#endif // CLIENTCONTROLLER_H
