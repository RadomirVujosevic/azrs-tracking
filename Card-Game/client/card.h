#ifndef CARD_H
#define CARD_H

#include <QString>
#include <QtDebug>
#include <QStringLiteral>

#include "cardnumber.h"
#include "cardsymbol.hpp"

class Card{
protected:
    CardSymbol symbol;
    CardNumber number;

public:
    //Karte se najcesce prave pomocu drugog konstruktora.
    //number oznacava broj karte, tako da se se kralj srce pravi pomocu poziva Card(CardSymbol::Hearts, 14) ili Card(CardSymbol::Hearts, CardNumber::King)
    //Kec se pravi sa arugmentom 1 ili 11
    Card(CardSymbol symbol, CardNumber number);
    Card(CardSymbol symbol, int number);
    Card();

    Card(QString symbol, QString number);
    Card(QString symbol, int number);

    CardSymbol getSymbol();
    CardNumber getNumber() const;

    QString toString();

    bool operator ==(Card c);
    bool operator !=(Card c);


    Card operator +(int n);

    bool operator <(const Card c) const;
};

#endif // CARD_H
