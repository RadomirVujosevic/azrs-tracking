#include "cardhand.h"

QString CardHand::HandsToString(Hands hand){
    QString handStrings [] ={
        "None", "HighCard", "Pair", "TwoPair", "ThreeOfAKind", "Straight", "Flush", "FullHouse", "FourOfAKind", "StraightFlush", "RoyalFlush"
    };
    return handStrings[hand];
}

void printVector(std::vector<Card> &vec) {
    qDebug() << "[";
    for(Card &c : vec){
        qDebug() << c.toString();
    }
    qDebug() << "]";
}



CardHand::CardHand(std::vector<Card> &communalCards, std::vector<Card> &playerCards){
    for(Card &c : communalCards){
        allCards.push_back(c);
    }
    for(Card &c : playerCards){
        allCards.push_back(c);
    }
    bestHandEvaluation = None;
    sort(allCards.begin(), allCards.end());

    evaluateHand(0, 5);
    sort(allCards.begin(), allCards.end());
}


void CardHand::evaluateHand(int offset, int k){
    if(k == 0){
        Hands evaluation = evaluateCombination(currentCombination);
        if(bestHandEvaluation == None || evaluation > bestHandEvaluation){

            bestHandEvaluation = evaluation;
            strongestHand = currentCombination;
        }
        else if(evaluation == bestHandEvaluation && compareEqualEvals(currentCombination)){

            strongestHand = currentCombination;
        }
        return;
    }
    for(ulong i = offset; i <= allCards.size() - k; ++i){
        currentCombination.push_back(allCards[i]);
        evaluateHand(i + 1, k - 1);
        currentCombination.pop_back();
    }
}



CardHand::Hands CardHand::evaluateCombination(std::vector<Card> &combination){
    bool straight = isStraight(combination);
    bool flush = isFlush(combination);
    cardCountMap = formCardMap(combination);

    if(flush && straight){
        if(combination[0].getNumber() == CardNumber::Ten){
            return RoyalFlush;
        }
        else return StraightFlush;
    }
    if(isFourOfAKind(combination)){
        return FourOfAKind;
    }
    if(isFullHouse( combination)){
        return FullHouse;
    }
    if(flush){
        return Flush;
    }
    if(straight){
        return Straight;
    }
    if(isThreeOfAKind(combination)){
        return ThreeOfAKind;
    }
    if(isTwoPair(combination)){
        return TwoPair;
    }
    if(isPair(combination)){
        return Pair;
    }
    return HighCard;

}

bool CardHand::isStraight(std::vector<Card> &combination){
    for(size_t i = 1; i < combination.size(); i++){
        if(combination[i - 1].getNumber() != CardNumber::King || combination[i].getNumber() != combination[i - 1].getNumber() + 1){
            return false;
        }
    }
    return true;
}

bool CardHand::isFlush(std::vector<Card> &combination){
    CardSymbol symbol = combination[0].getSymbol();
    for(size_t i = 1; i < combination.size(); i++){
        if(combination[i].getSymbol() != symbol){
            return false;
        }
    }
    return true;
}

bool CardHand::isFullHouse(std::vector<Card> &combination){
    return isThreeOfAKind(combination) && isPair(combination);
}

bool CardHand::isFourOfAKind(std::vector<Card> &combination){
    for(auto &pair: cardCountMap){
        if(pair.second == 4)
            return true;
    }
    return false;
}

bool CardHand::isThreeOfAKind(std::vector<Card> &combination){
    for(auto &pair: cardCountMap){
        if(pair.second == 3)
            return true;
    }
    return false;
}

bool CardHand::isTwoPair(std::vector<Card> &combination){
    bool firstPairFound = false;
    for(auto &pair: cardCountMap){
        if(pair.second == 2){
            if(firstPairFound == false){
                firstPairFound = true;
            }else{
                return true;
            }
        }
    }
    return false;
}

bool CardHand::isPair(std::vector<Card> &combination){
    for(auto &pair: cardCountMap){
        if(pair.second == 2)
            return true;
    }
    return false;
}

bool CardHand::operator<(CardHand &rhs){
    if  (bestHandEvaluation < rhs.bestHandEvaluation) return true;

    if  (bestHandEvaluation > rhs.bestHandEvaluation) return false;

    return compareEqualEvals(rhs.strongestHand);
}

bool CardHand::compareEqualEvals(std::vector<Card> &rCards){
    bool result;
    sort(strongestHand.begin(), strongestHand.end());
    sort(rCards.begin(), rCards.end());
    cardCountMap = formCardMap(strongestHand);
    rCardCountMap = formCardMap(rCards);



    switch (bestHandEvaluation){
    case None:
        qDebug() << "Tried to compare evals when they are None - should be impossible";
        result = false;
        break;
    case HighCard:
        result = compareHighCard(rCards);
        break;
    case Pair:
        result = comparePair(rCards);
            break;
    case TwoPair:
        result = compareTwoPair(rCards);
        break;
    case ThreeOfAKind:
        result = compareThreeOfAKind(rCards);
        break;
    case FourOfAKind:
        result = compareFourOfAKind(rCards);
        break;
    case FullHouse:
        result = compareFullHouse(rCards);
        break;
    case RoyalFlush:
        qDebug() << "Tried to compare evals when they are None - should be impossible";
        result = false;
        break;
    case Straight:
        result = compareStraight(rCards);
        break;
    case StraightFlush:
        result = compareStraightFlush(rCards);
        break;
    case Flush:
        result = compareFlush(rCards);
        break;
    }
    return result;
}

bool CardHand::compareHighCard(std::vector<Card> &rCards){
    for(long i = strongestHand.size() - 1; i >= 0; i--){
        if(strongestHand[i].getNumber() != rCards[i].getNumber()){
            return strongestHand[i] < rCards[i];
        }
    }
    return true;
}

bool CardHand::comparePair(std::vector<Card> &rCards){
    CardNumber myNumber;
    CardNumber rNumber;
    for(auto &pair : cardCountMap){
        if(pair.second == 2){
            myNumber = pair.first.getNumber();
            break;
        }
    }
    for(auto &pair : rCardCountMap){
        if(pair.second == 2){
            rNumber = pair.first.getNumber();
            break;
        }
    }
    if(myNumber != rNumber){
        return myNumber < rNumber;
    }
    else{
        return compareHighCard(rCards);
    }
}

bool CardHand::compareThreeOfAKind(std::vector<Card> &rCards){
    CardNumber myNumber;
    CardNumber rNumber;
    for(auto &pair : cardCountMap){
        if(pair.second == 3){
            myNumber = pair.first.getNumber();
            break;
        }
    }
    for(auto &pair : rCardCountMap){
        if(pair.second == 3){
            rNumber = pair.first.getNumber();
            break;
        }
    }
    if(myNumber != rNumber){
        return myNumber < rNumber;
    }
    else{
        return compareHighCard(rCards);
    }
}

bool CardHand::compareFourOfAKind(std::vector<Card> &rCards){
    CardNumber myNumber;
    CardNumber rNumber;
    for(auto &pair : cardCountMap){
        if(pair.second == 4){
            myNumber = pair.first.getNumber();
            break;
        }
    }
    for(auto &pair : rCardCountMap){
        if(pair.second == 4){
            rNumber = pair.first.getNumber();
            break;
        }
    }
    if(myNumber != rNumber){
        return myNumber < rNumber;
    }
    else{
        return compareHighCard(rCards);
    }
}

bool CardHand::compareTwoPair(std::vector<Card> &rCards){
    CardNumber myPair1;
    CardNumber myPair2;

    CardNumber rPair1;
    CardNumber rPair2;

    bool firstPairFound = false;
    for(auto &pair : cardCountMap){
        if(pair.second == 2){
            if(firstPairFound){
                myPair2 = pair.first.getNumber();
                if(myPair2 > myPair1){
                    myPair2 = myPair1;
                    myPair1 = pair.first.getNumber();
                }
            }
            else{
                myPair1 = pair.first.getNumber();
                firstPairFound = true;
            }
        }
    }

    firstPairFound = false;
    for(auto &pair : rCardCountMap){
        if(pair.second == 2){
            if(firstPairFound){
                rPair2 = pair.first.getNumber();
                if(rPair2 > rPair1){
                    rPair2 = rPair1;
                    rPair1 = pair.first.getNumber();
                }
            }
            else{
                rPair1 = pair.first.getNumber();
                firstPairFound = true;
            }
        }
    }

    if(myPair1 != rPair1){
        return myPair1 < rPair1;
    }

    if(myPair2 != rPair2){
        return myPair2 < rPair2;
    }

    return compareHighCard(rCards);
}

bool CardHand::compareStraight(std::vector<Card> &rCards){
   return strongestHand.back() < rCards.back();
}

bool CardHand::compareFlush(std::vector<Card> &rCards){
    return compareHighCard(rCards);
}

bool CardHand::compareFullHouse(std::vector<Card> &rCards){
    CardNumber myPair;
    CardNumber myTrio;

    CardNumber rPair;
    CardNumber rTrio;
    for(auto &pair : cardCountMap){
        if(pair.second == 2){
            myPair = pair.first.getNumber();
            break;
        }
        else if(pair.second == 3){
            myTrio = pair.first.getNumber();
        }
    }
    for(auto &pair : rCardCountMap){
        if(pair.second == 2){
            rPair = pair.first.getNumber();
            break;
        }
        else if(pair.second == 3){
            rTrio = pair.first.getNumber();
        }
    }

    if(myTrio != rTrio){
        return myTrio < rTrio;
    }

    return myPair < rPair;
}

bool CardHand::compareStraightFlush(std::vector<Card> &rCards){
    return compareStraight(rCards);
}





std::map<Card, int> CardHand::formCardMap(std::vector<Card> &combination){
    std::map<Card, int> cardCountMap;
    for(Card &c : combination){
        if(cardCountMap.find(c) != cardCountMap.end()){
            cardCountMap[c] = cardCountMap[c] + 1;
        }else{
            cardCountMap[c] = 1;
        }
    }
    return cardCountMap;
}

QString CardHand::toString(){
    QString result = QStringLiteral("All cards: [");
    for(int i = 0; i < allCards.size(); i++){
        if(i != 0){
            result.append(", ");
        }
        result.append(allCards[i].toString());
    }
    result.append("]\n");
    result.append("Strongest combination is [");
    for(long i = 0; i < strongestHand.size(); i++){
        if(i != 0){
            result.append(", ");
        }
        result.append(strongestHand[i].toString());
    }
    result.append("] - ").append( HandsToString(bestHandEvaluation));
    return result;
}
