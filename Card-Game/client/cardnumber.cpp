#include "cardnumber.h"

QString CardNumber::numberToString(numbers number){
    QString numberStrings[13] = {
        "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"
    };
    return numberStrings[number];
}

QString CardNumber::numberToString(CardNumber number){
    QString numberStrings[13] = {
        "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"
    };
    return numberStrings[number.number];
}

CardNumber CardNumber::stringToNumber(QString number){
    int idx = 0;
    const unsigned cardCount = 13;

    QString numberStrings[cardCount] = {
        "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king", "ace"
    };

    number = number.trimmed().toLower();

    for(idx = 0; idx < cardCount; idx++) {
        if(number.compare(numberStrings[idx]) == 0) break;
    }

    if(idx < 0 || idx >= cardCount) {
        qDebug() << "invalid string to number conversion";
        throw std::invalid_argument(number.toStdString() + " is an invalid argument to CardNumber::stringToNumber()");
    }
    CardNumber cn{numbers(idx)};
    return cn;
}

CardNumber CardNumber::intToNumber(int number){
    if(number < 1 || number > 14){
        qDebug() << "Incorrect card number";
        throw std::domain_error("number either lower than 1 or larger than 14 in intToNumber()");
    }
    if (number >= 2 && number <= 10){
        CardNumber cn(numbers(number - 2));
        return cn;
    }
    if(number == 1 || number == 11){
        CardNumber cn(numbers::Ace);
        return cn;
    }
    CardNumber cn(numbers(number - 3));
    return cn;

}

QString CardNumber::toString(){
    return numberToString(number);
}

int CardNumber::toInt(){
    if(number == Ace){
        return 1;
    }
    if(number >= Two && number <= Ten){
        return number + 2;
    }
    else{
        return number + 3;
    }
}

bool CardNumber::operator ==(CardNumber cn){
    return number == cn.number;
}

bool CardNumber::operator !=(CardNumber cn){
    return number != cn.number;
}

bool CardNumber::operator >(const CardNumber cn)const {
    return number > cn.number;
}

bool CardNumber::operator <(const CardNumber cn)const {
    return number < cn.number;
}

CardNumber CardNumber::operator+(int n){
    if((CardNumber(number).toInt() + n) > 14) throw std::overflow_error("cardNumber + int overflow ie result is over 14/King");
    return CardNumber(numbers(number + n));
}

CardNumber CardNumber::operator-(int n){
    if((CardNumber(number).toInt() - n) < 1) throw std::overflow_error("cardNumber - int underflow ie result is under 1/Ace");
    return CardNumber(numbers(number - n));
}
