#ifndef PLAYER_H
#define PLAYER_H

#include "card.h"
#include "aiaction.h"
#include "QThread"
#include <QObject>

class Player {
public:
    enum playerTypes{
        AI, Human
    };

    enum playerStatus{
        Folded, Deciding, Checked, AllIn
    };

    Player(int id, int money, QString username, playerTypes type = Human);
    Player();

    //Getteri
    int getBet() const;
    playerStatus getStatus() const;
    int getID() const;
    int getMoney() const;
    QString getUsername() const;
    playerTypes getPlayerType() const;
    std::vector<Card> &getCards();

    //Setteri
    void setStatus(playerStatus ps);

    QString toString();

    void changeMoney(int n);

    //Poziva se kada se potvrdi da igrac hoce ulozi novac (Raise ili Call). Prebacuje se novac iz money u currentBet
    int makeBet(int bet);

    /*
    Funkcija koja se poziva kada je igrac na redu i od njega se ocekuje da ce odgovoriti sa brojem koji oznacava koliko para hoce da ulozi
    Ukoliko igrac vrati broj koji je jednak bet, to se smatra kao Call
    Ukoliko igrac vrati broji koji je veci od bet, to se smatra kao Raise
    Ukoliko igrac vrati broj koji je manji od bet, ali je jednak preostalom novcu, to se smatra kao All in
    U suprotnom, koji god broj da se vrati, smatrace se za Fold i nece se oduzeti novac od trenutnog igraca
    */
    int getResponse( std::vector<Card> &communalCards, int bet, int notFoldedCount);

    //Vraca currentBet na 0
    void resetBet();

    void resetRaised();

    bool hasFolded();

    void makeDecision(std::vector<Card> &communalCards, int bet);

    bool finishedTurn();

    void setMoney(int money);

    bool operator ==(Player &p);

private:
    //Karte koje igrac ima. Ukoliko nema bugova uvek ce imati 0 ili 2 karte
    //TODO: Ubaciti funkcije addCard i removeCard koje ce da vrse provere i izbace greske ako igrac ima previse ili premalo karti
    std::vector<Card> cards;

    //ID igraca jedinstveno oznacava igraca. Za sada se koristi kao ime
    int id;

    //Koliko novca igrac ukupno ima. Ne ukljucuje currentBet
    int money;

    bool raised;

    //Koliko je novca igrac ulozio u rundi
    int currentBet;
    QString username;
    enum playerStatus status;
    enum playerTypes playerType;

signals:
    void sendResponse(int id, int bet);
public slots:
    void decideBet(std::vector<Card> &communalCards, int bet);
};

#endif // PLAYER_H
