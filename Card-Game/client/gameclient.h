#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QObject>
#include <QTcpSocket>
#include "player.h"

class QHostAddress;
class QJsonDocument;
class GameClient : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameClient)
public:
    explicit GameClient(QObject *parent = nullptr);
public slots:
    void connectToServer(const QHostAddress &address, quint16 port);
    void login(const QString &userName);
    void disconnectFromHost();
    void betRequest(int bet);
private slots:
    void onReadyRead();
signals:
    void connected();
    void loggedIn(int id);
    void loginError(const QString &reason);
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);
    void betReceived(int id, int bet);
    void gameStarted(QVector<Player>& players, int startingMoney);
    void foldReceived(int id);
    void playerAdded(int id, QString username);
    void playerRemoved(int id);
    void roundStarted();
    void cardsReceived(QString symbol1, int number1, QString symbol2, int number2);
    void flopReceived(QString symbol1, int number1, QString symbol2, int number2, QString symbol3, int number3);
    void turnReceived(QString symbol, int number);
    void riverReceived(QString symbol, int number);
    void waitingForInput(int id, int bet);
    void playerToMove(int id);
    void changeMoney(int id, int money);
    void gameEnded();
    void showCards(int id, QString symbol1, int number1, QString symbol2, int number2);
    void playerWon(int id);
private:
    QTcpSocket *m_clientSocket;
    bool m_loggedIn;
    QVector<Player> players;
    void jsonReceived(const QJsonObject &doc);
};

#endif // GAMECLIENT_H
