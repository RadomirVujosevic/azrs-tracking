#include "aiaction.h"

void AIAction::setProbability() {
  probability[AIAction::HighCard] = 50.1177;
  probability[AIAction::Pair] = 42.2569;
  probability[AIAction::TwoPair] = 4.7539;
  probability[AIAction::ThreeOfAKind] = 2.1128;
  probability[AIAction::Straight] = 0.3925;
  probability[AIAction::Flush] = 0.1965;
  probability[AIAction::FullHouse] = 0.1441;
  probability[AIAction::FourOfAKind] = 0.02401;
  probability[AIAction::StraightFlush] = 0.00139;
  probability[AIAction::RoyalFlush] = 0.000154;
}

void AIAction::setComulative() {
  comulative[AIAction::HighCard] = 100;
  comulative[AIAction::Pair] = 49.9;
  comulative[AIAction::TwoPair] = 7.62;
  comulative[AIAction::ThreeOfAKind] = 2.87;
  comulative[AIAction::Straight] = 0.76;
  comulative[AIAction::Flush] = 0.367;
  comulative[AIAction::FullHouse] = 0.17;
  comulative[AIAction::FourOfAKind] = 0.0256;
  comulative[AIAction::StraightFlush] = 0.0015;
  comulative[AIAction::RoyalFlush] = 0.000154;
}

AIAction::AIAction(std::vector<Card> &handCards, std::vector<Card> &commulativeCards) : handCards(handCards), commulativeCards(commulativeCards) {
  combination.insert(combination.end(), handCards.begin(), handCards.end());
  combination.insert(combination.end(), commulativeCards.begin(), commulativeCards.end());
  setProbability();
  setComulative();
}

int AIAction::calculateMove() {
  handValue evaluatedHand;

  if (commulativeCards.size() > 2)
    evaluatedHand = evaluateCombination(combination);
  else evaluatedHand = AIAction::HighCard;

  removeImpossibleCombinations();

  if (comulative[evaluatedHand] < 20 || commulativeCards.size() == 0) {
    return 2;
  } else if (comulative[evaluatedHand] < 40 || combination.size() < 6) {
    return 1;
  } else return 0;
}

std::map<Card, int> AIAction::formCardMap(std::vector<Card> &combination){
  std::map<Card, int> cardCountMap;
  for(Card &c : combination){
    if(cardCountMap.find(c) != cardCountMap.end()){
      cardCountMap[c] = cardCountMap[c] + 1;
    }else{
      cardCountMap[c] = 1;
    }
  }
  return cardCountMap;
}

bool AIAction::isStraight(std::vector<Card> &combination){
	for(size_t i = 1; i < combination.size(); i++){
		if(combination[i-1].getNumber() == CardNumber::intToNumber(14)) {
			return false;
		}
		if(combination[i].getNumber() != combination[i - 1].getNumber() + 1){
			return false;
		}
	}

	return true;
}

bool AIAction::isFlush(std::vector<Card> &combination){
  CardSymbol symbol = combination[0].getSymbol();
  for(size_t i = 1; i < combination.size(); i++){
    if(combination[i].getSymbol() != symbol){
      return false;
    }
  }
  return true;
}

bool AIAction::isFullHouse(std::vector<Card> &combination, std::map<Card, int> cardCountMap){
  return isThreeOfAKind(combination, cardCountMap) && isPair(combination, cardCountMap);
}

bool AIAction::isFourOfAKind(std::vector<Card> &combination, std::map<Card, int> cardCountMap){
  for(auto &pair: cardCountMap){
    if(pair.second == 4)
      return true;
  }
  return false;
}

bool AIAction::isThreeOfAKind(std::vector<Card> &combination, std::map<Card, int> cardCountMap){
  for(auto &pair: cardCountMap){
    if(pair.second == 3)
      return true;
  }
  return false;
}

bool AIAction::isTwoPair(std::vector<Card> &combination, std::map<Card, int> cardCountMap){
  bool firstPairFound = false;
  for(auto &pair: cardCountMap){
    if(pair.second == 2){
      if(firstPairFound == false){
        firstPairFound = true;
      }else{
        return true;
      }
    }
  }
  return false;
}

bool AIAction::isPair(std::vector<Card> &combination, std::map<Card, int> cardCountMap){
  for(auto &pair: cardCountMap){
    if(pair.second == 2)
      return true;
  }
  return false;
}

void AIAction::removeImpossibleCombinations() {
  std::map<Card, int> cardCountMap = formCardMap(commulativeCards);

  if (!isPair(commulativeCards, cardCountMap)) {
    for (int i=0; i<AIAction::StraightFlush; i++) {
      comulative[(AIAction::handValue) i] -= (probability[AIAction::FourOfAKind] + probability[AIAction::FullHouse]);
    }
  }
}

AIAction::handValue AIAction::evaluateCombination(std::vector<Card> &combination){
  bool straight = isStraight(combination);
  bool flush = isFlush(combination);
  std::map<Card, int> cardCountMap = formCardMap(combination);
  if(flush && straight){
    if(combination[0].getNumber() == CardNumber::Ten){
      return RoyalFlush;
    }
    else return StraightFlush;
  }
  if(isFourOfAKind(combination, cardCountMap)){
    return FourOfAKind;
  }
  if(isFullHouse(combination, cardCountMap)){
    return FullHouse;
  }
  if(flush){
    return Flush;
  }
  if(straight){
    return Straight;
  }
  if(isThreeOfAKind(combination, cardCountMap)){
    return ThreeOfAKind;
  }
  if(isTwoPair(combination, cardCountMap)){
    return TwoPair;
  }
  if(isPair(combination, cardCountMap)){
    return Pair;
  }
  return HighCard;
}
