#ifndef CARDNUMBER_H
#define CARDNUMBER_H

#include <QString>
#include <QtDebug>
#include <stdexcept>

class CardNumber
{
public:
    enum numbers{
        Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
    };

    CardNumber(numbers s):number(s){}
    CardNumber(){}

    static QString numberToString(numbers number);
    static QString numberToString(CardNumber number);
    static CardNumber stringToNumber(QString number);
    static CardNumber intToNumber(int number);


    QString toString();
    int toInt();

    bool operator ==(CardNumber cn);
    bool operator !=(CardNumber cn);
    bool operator >(const CardNumber cn)const;
    bool operator <(const CardNumber cn)const;

    CardNumber operator +(int n);
    CardNumber operator -(int n);

private:
    numbers number;

};

#endif // CARDNUMBER_H
