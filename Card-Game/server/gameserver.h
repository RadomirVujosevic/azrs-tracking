#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QTcpServer>
#include <QVector>
#include "client/player.h"

class QThread;
class ServerWorker;
class GameServer : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(GameServer)
public:
    explicit GameServer(QObject *parent = nullptr);
    void startGame(QVector<Player>& players, int startingMoney);
    void startRound();
    void betMade(int id, int bet);
    void playerFolded(int id);
    void sendCards(int id, QString symbol1, int number1, QString symbol2, int number2);
    void sendFlop(QString symbol1, int number1, QString symbol2, int number2, QString symbol3, int number3);
    void sendTurn(QString symbol, int number);
    void sendRiver(QString symbol, int number);
    void waitingForInput(int id, int bet);
    void playerToMove(int id);
    void changeMoney(int id, int money);
    void gameEnded();
    void addPlayer(int id, QString username);
    void removePlayer(int id);
    void showCards(int id, QString symbol1, int number1, QString symbol2, int number2);
    void playerWon(int id);
    void incrementId();
protected:
    void incomingConnection(qintptr socketDescriptor) override;
signals:
    void logMessage(const QString &msg);
    void betReceived(int id, int bet);
    void playerAdded(QString username);
    void playerRemoved(int id);
public slots:
    void stopServer();
private slots:
    void broadcast(const QJsonObject &message, ServerWorker *exclude);
    void broadcast(const QJsonObject &message, int id);
    void jsonReceived(ServerWorker *sender, const QJsonObject &doc);
    void userDisconnected(ServerWorker *sender);
    void userError(ServerWorker *sender);
private:
    void jsonFromLoggedOut(ServerWorker *sender, const QJsonObject &doc);
    void jsonFromLoggedIn(ServerWorker *sender, const QJsonObject &doc);
    void sendJson(ServerWorker *destination, const QJsonObject &message);
    void sendJson(int id, const QJsonObject &message);
    QVector<ServerWorker *> m_clients;
    bool m_gameStarted = 0;
    QStringList m_players;
    QList<int> m_ids;
    int nextId = 1;
};

#endif // GAMESERVER_H
