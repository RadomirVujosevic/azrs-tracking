#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H

#include <QWidget>
#include <QObject>
#include "client/card.h"
#include "client/cardhand.h"

namespace Ui {
class ServerController;
}
class GameServer;
class ServerController : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ServerController)
public:
    explicit ServerController(QWidget *parent = nullptr, int startingMoney=500, int bigBlind=10);
    ~ServerController();
    /* kopirano iz servercontroller.hpp */
    enum Stages{
        Preflop, Flop, Turn, River
    };

    //U deck se na pocetku stavlja svih 52 karte, nakon cega se one izvlace tokom runde i vrate se u deck na kraju
    std::vector<Card> deck;
    std::vector<Card> communalCards;

    //Klasa Player cuva informacije o pojedinacnim igracima - koje karte imaju, koliko para imaju itd
    //Server ima jedan Player objekat za svaki GameClient objekat
    //U principu su ovo kopije - clients[0].player i players[0] imaju iste vrednosti za sva polja
    //Cuvaju se dve kopije radi bezbednosti - za slucaj da neki igrac nadje hack koji mu omogucava da promeni vrednosti u programu.
    //U ovom slucaju se menja samo lokalna vrednost, a vrednost koja je zapravo bitna se nalazi u GameServer kopiji ove klase
    std::vector<Player> players;

    //Odigra trenutni stage - prvo deli karte, pa obradi opklade, pa onda odradi deo vezan za kraj stage - videti implementaciju
    void playStage();

    unsigned getPlayerCount();

private:
    //Svi igraci moraju imati jedinstven ID
    //Kada se napravi novi igrac, ovo se povecava za 1
    //U implementaciji ServerController moze da referise na pojedinacne igrace preko indeksa - players[i], ali kada se salju signali sigurnije je da se koriste ID
    int nextID = 1;

    //Trenutni stage
    Stages stage;

    //Broj botova u partiji
    int botCount;

    //Izabrati random imena za botove sa ovog spiska
    //TODO: Smisliti normalna imena
    std::vector<QString> botNames = {"Pera", "Zika", "Marko Markovic", "Mika", "Bot McBot", "yazuuchi", "lorenza", "BB", "CC", "02", "2B", "A2", "9S", "Leo", "Crni Milos", "Ivo", "Fjodor", "Bora"};

    //Menja se status svih igraca u Deciding. Ovo se radi samo na pocetku runde
    void setAllPlayersDeciding();

    //Isto kao prethodna funkcija, samo se ne menja status ako je vec Folded.
    //Igrac koji je u ovom stanju je jos uvek prisutan za stolom, ali ne igra ovu rundu, pa ga treba ignorisati
    void setCheckedPlayersDeciding();

    //Ako su svi igraci u stanju Checked ili Folded onda su sve opklade obradjene i moze da se predje na sledeci stage
    bool allPlayersFinishedTurn();

    //Svaki igrac cuva currentBet promenljivu, a ova funkcija postavlja ovu promenljivu na 0 za sve igrace
    void resetBets();

    //Cuva se trenutni igrac i omogucava se prelaz na sledeceg igraca
    int playerToMoveIndex;
    //Pokazivac nije neophodan ali olaksava implementaciju - lepse mi je da pisem playerToMove-> nego players[playerToMoveIndex], ali je isti djavo
    Player* playerToMove;
    void nextPlayer();

    //U zavisnosti od toga koji je trenutni stage, dele se karte igracima, ili se dodaju karte na sto
    void dealCards();

    //Funkcija se poziva samo jednom - kada se ona pozove, server se sprema da obradi opklade
    //Ovde se zavrsava sinhroni rad servera i prelazi u stanje cekanja dok neki igrac ne pokusa da unese opkladu
    //Rad se nastavlja iskljucivo kada se pozove receivedBetsSlot()
    void proccessBets();

    //Zavrsava se trenutni stage i prelazi na sledeci
    //Ukoliko je trentuno River(poslednji stage), bira se pobednik i prelazi se na novu rundu
    void endStage();

    //Bira se pobednik i vraca se njegov indeks u vektoru players
    int decideWinner();

    //Pomera Kartu iz origin vektora u dest. Ovo radi tako sto pravi kopiju, ubaci je u dest, a zatim izbrise iz origin
    //Neefikasan pristup - verovatno bi bilo bolje preko unique_ptr, ali se poziva relativno mali broj puta - 10 + 4 * playerCount za svaku rundu.
    //Pored toga, Card objekti ne zauzimaju mnogo memorije.
    void moveCard(std::vector<Card> &origin, std::vector<Card> &dest, int idx);
    void moveRandomCards(std::vector<Card> &origin, std::vector<Card> &dest, int count = 1);

    //Trazi igraca i vraca njegov indeks u vektoru players ako je taj igrac pronadjen, -1 ako nije
    int findPlayer(Player &p);
    int findPlayer(int id);

    //Ispisuje sve igrace i karte tako sto poziva njihove toString() metode
    //Koristi se samo za debug
    void writePlayers();
    void writeCommunalCards();

    //Brise igraca iz vektora players. Poziva se kada na kraju runde neki igrac ima 0 novca ili ako igrac odluci da prestane da igra
    void removePlayer(Player &p);

    //Vraca broj igraca koji nisu u stanju Folded
    int notFoldedCount();

    //Vraca karte od svih igraca i iz communalCards nazad u vektor
    void returnCardsToDeck();

    // Vraca indeks prvog igraca koji nije u stanje Folded
    int findNotFoldedPlayer();

    //Proverava da li neki od igraca ima 0 novca i izbacuje ga
    void removePlayersWithoutMoney();

    //Proverava da li je opklada ispravna
    bool betIsValid(int id, int bet);

    //Vraca broj igraca koji nisu u stanju AllIn ili Folded - ovo su igraci koji su jos aktivni u rundi
    int activePlayerCount();

    //Dele se karte na sto, dok na stolu nema 5 karti
    void dealRemainingCards();

    //Opklada koji svi igraci moraju da prate ili da folduju
    int betPerPlayer;

    //Koliko treba da plati igrac koji placa big ili small blind
    int bigBlind;
    int smallBlind;

    //pot odredjuje koliko je para ukupno u opticaju trenutno. Na kraju partije, citav pot se dodeljuje pobedniku i restuje na 0
    int pot;

    //Indeks igraca koji placa bigBlind. Na kraju svake runde se pomera na sledeceg, tj. uvecava se za 1
    int bigBlindIndex;

    //Indeks igraca koji placa smallBlind. On je uvek bigBlindIndex - 1
    int smallBlindIndex;

    //Koliko novca igraci imaju na pocetku
    int startingMoney;

    bool waitingForBets = false;
    /* kraj kopiranog dela*/
    Ui::ServerController *ui;
    GameServer *m_gameServer;

protected:
    void closeEvent(QCloseEvent *event) override;

signals:
    void windowClosed();
    void playerCountChanged(unsigned playerCount);

public:
    bool isServerStarted() const;

public slots:
    void toggleStartServer();
    void startGame(unsigned botCount = 0);

private slots:
    void startRound();
    void playerAdded(QString username);
    void playerRemoved(int id);
    void betReceived(int id, int bet);
    void logMessage(const QString &msg);
    void nextRound();
};

#endif // SERVERCONTROLLER_H
