#ifndef WIDGET_H
#define WIDGET_H

#pragma once

#include <QWidget>
#include "qfile.h"

#include <QPixmap>
#include <QTime>
#include <QtDebug>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QDebug>
#include <QSettings>

#include <vector>

#include "server/servercontroller.h"
#include "client/player.h"
#include "layouts/cardlayout.h"
#include "layouts/playerlayout.h"
#include "layouts/tablelayout.h"
#include "ui/layouts/betinput.h"
#include "ui/soundmanager.h"

#define DEBUG

#ifdef DEBUG
    // Circular dependency
    #include "client/clientcontroller.h"
    #include <QApplication>
    #include <QDesktopWidget>
    #include <QWindow>

    class ClientController;
#endif

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QString getName();

#ifdef DEBUG
private slots:
    void createClient();
private:
    std::vector<ClientController*> ccs = std::vector<ClientController*>(0);
    QPushButton *btn = nullptr;
#endif

public slots:
    void gameStarted(int playerCount, int localID, const QVector<Player> &players, int initialMoney);
    void playerBetChanged(int id, int bet);
    void potChanged(int pot);
    void tableCardsChanged(const std::vector<Card> &cards);
    void playerMoneyChanged(int id, int currentMoney);
    void playerCardsChanged(int id, const std::vector<Card> &cards);
    void playerLost(int id);
    void playerFolded(int id);
    void roundStarted();
    void gameEnded();
    void requestBetSlot(int callAmount);
    void returnBetSlot(int bet);
    void playerWon(int id);
    void serverExited();
    void hostTerminatedConnection();

signals:
    void attemptConnection();
    void attemptConnectionToAddress(QString hostAddress);
    void returnBetToClient(int bet);
    void requestBetFromBetInput(int callAmount, int currentMoney);
    void playerQuitSignal();

private:
    Ui::Widget *ui;

    const static inline QString localAddress{"127.0.0.1"};

    const static unsigned minCardHeight = 50;
    const static unsigned maxCardHeight = 499;
    unsigned cardHeight = 128;

    unsigned playerCount = 4;
    const static unsigned maxPlayers = 10;
    const static unsigned minPlayers = 2;

    const static int lostID = -1;

    static inline QSettings settings{"MATF studenti", "Card Game"};
    SoundManager sm{};

    QValidator *moneyValidator = nullptr;
    QValidator *blindValidator = nullptr;

    TableLayout *tableLayout = nullptr;
    PlayerLayout *localPlayer = nullptr;
    std::vector<PlayerLayout*> playerLayouts{};

    QPixmap *blankCard = nullptr;
    QPixmap *invisCard = nullptr;
    QPixmap *jokerCard = nullptr;
    std::vector<std::vector<QPixmap>> cardImages =
            std::vector<std::vector<QPixmap>>(4, std::vector<QPixmap>(0));

    ServerController *sc = nullptr;

    void initializeLayouts();
    // There was a bug when QPixmaps were intialized when declared or
    // in the constructor, and this function is the solution to remove it
    void initializeQPixmaps();
    // adds tableLayouts and playerLayouts to the singlePlayerMenuWidget
    void showLayouts();

    unsigned getIndexOfId(int id);
    void showMessageBox(QString title, QString text);
    void messagePlayer(QString msg);
    void updateServerPlayerCountLabel(unsigned count);
    void resetGameBoard();
    void deleteServer();

private slots:
    // TODO: Connect by name je lose?
    void on_optionsButton_clicked();
    void on_exitButton_clicked();
    void on_optionsBackButton_clicked();
    void on_singleplayerButton_clicked();
    void on_spBack_clicked();
    void on_startGameButton_clicked();
    void on_playerCountPicker_valueChanged(int arg1);
    void on_backToStartButton_clicked();
    void on_hostButton_clicked();
    void on_saveNameButton_clicked();
    void on_joinButton_clicked();
    void on_lonerGameButton_clicked();
    void on_musicSlider_valueChanged(int value);
    void on_soundSlider_valueChanged(int value);
    void on_Widget_destroyed();
    void on_cardHeightEdit_valueChanged(int arg1);
};
#endif // WIDGET_H
