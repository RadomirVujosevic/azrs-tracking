#include "betinput.h"
#include "ui_betinput.h"

BetInput::BetInput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BetInput)
{
    ui->setupUi(this);

    disableInput();
}

BetInput::~BetInput()
{
    if(validator != nullptr) delete validator;
    delete ui;
}

void BetInput::requestBet(int callAmount, int currentMoney) {
    money = currentMoney;

	if(callAmount > money) callAmount = money;

    if(validator == nullptr) {
        validator = new QIntValidator(callAmount, money);
    } else {
        validator->setTop(money);
        validator->setBottom(callAmount);
    }

    ui->betInputEdit->setValidator(validator);
    ui->betInputEdit->setText(QString("%1").arg(callAmount));

    enableInput();
}

void BetInput::buttonClick(int betAmount) {
    ui->betInputEdit->clear();
    disableInput();
    emit bet(betAmount);
}

void BetInput::enableInput()
{
    ui->betInputEdit->setEnabled(true);
    ui->foldButton->setEnabled(true);
    ui->callButton->setEnabled(true);
    ui->raiseButton->setEnabled(true);
}

void BetInput::disableInput()
{
    ui->betInputEdit->setEnabled(false);
    ui->foldButton->setEnabled(false);
    ui->callButton->setEnabled(false);
    ui->raiseButton->setEnabled(false);
}

void BetInput::on_foldButton_clicked()
{
    buttonClick(-1);
}

void BetInput::on_callButton_clicked()
{
    buttonClick(validator->bottom());
}

void BetInput::on_raiseButton_clicked()
{
    int amount = ui->betInputEdit->text().toInt();

    if(amount > money) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Wrong bet input");
        msgBox.setText("Bet amount shouldn't exceed current player money.");
        msgBox.exec();
    }
    else {
        buttonClick(amount);
    }
}
