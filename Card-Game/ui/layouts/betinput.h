#ifndef BETINPUT_H
#define BETINPUT_H

#include <QWidget>
#include <QIntValidator>
#include <QMessageBox>
#include <QDebug>

namespace Ui {
class BetInput;
}

class BetInput : public QWidget
{
    Q_OBJECT

public:
    explicit BetInput(QWidget *parent = nullptr);
    ~BetInput();

signals:
    void bet(int amount);

public slots:
    void requestBet(int callAmount, int currentMoney);

private slots:
    void on_foldButton_clicked();
    void on_callButton_clicked();
    void on_raiseButton_clicked();

private:
    Ui::BetInput *ui;
    QIntValidator *validator = nullptr;
    int money = 0;

    void enableInput();
    void disableInput();
    void buttonClick(int betAmount);
};

#endif // BETINPUT_H
