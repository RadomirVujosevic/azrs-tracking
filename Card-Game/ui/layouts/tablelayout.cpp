#include "tablelayout.h"

TableLayout::TableLayout(unsigned money, QWidget *parent)
    : CardLayout{money, cardNumber, parent}
{
    layout->addWidget(moneyLabel, 0, 0, 1, 2, Qt::AlignLeft | Qt::AlignBottom);
}

void TableLayout::setMoney(const unsigned newMoney)
{
    money = newMoney;
    QString text = QString("Pot: %1").arg(money);
    moneyLabel->setText(text);
}
