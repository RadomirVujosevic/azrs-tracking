#ifndef TABLELAYOUT_H
#define TABLELAYOUT_H

#include <QObject>
#include <QWidget>
#include "ui/layouts/cardlayout.h"

class TableLayout : public CardLayout
{
    Q_OBJECT
private:
    const static unsigned cardNumber = 5;

public:
    TableLayout(unsigned money = 10000, QWidget *parent = nullptr);

    void setMoney(const unsigned newMoney);
};

#endif // TABLELAYOUT_H
