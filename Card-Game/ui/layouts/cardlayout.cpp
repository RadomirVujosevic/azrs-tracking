#include "cardlayout.h"

extern QPixmap *blankCard;
extern std::vector<std::vector<QPixmap>> cardImages;
extern unsigned cardHeight;

void CardLayout::setCardImages(QPixmap *newBlankCard, QPixmap *newInvisCard,
                    const std::vector<std::vector<QPixmap>> &newCardImages) {
    CardLayout::blankCard = newBlankCard;
    CardLayout::invisCard = newInvisCard;
    CardLayout::cardImages = newCardImages;
}

CardLayout::CardLayout(unsigned money, unsigned cardNumber, QWidget *parent) :
    QWidget(parent), cardNumber(cardNumber), money(money)
{
    layout = new QGridLayout(this);
    moneyLabel = new QLabel(QString("Money: 0"));

    if(cardNumber == 0) cardNumber = 2;
    cardLayout = new QHBoxLayout();
    cards = std::vector<QLabel*>(0, new QLabel(QString("")));

    for(unsigned i = 0; i < cardNumber; i++) {
        auto newCard = new QLabel(QString(""));

        newCard->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

        cards.push_back(newCard);
    }

    for(unsigned i = 0; i < cardNumber; i++) {
        cardLayout->addWidget(cards[i], Qt::AlignLeft | Qt::AlignBottom);
        hideCard(cards[i]);
    }

    layout->addWidget(moneyLabel, 0, 0, 1, 2, Qt::AlignLeft | Qt::AlignBottom);
    layout->addLayout(cardLayout, 1, 0, 1, 2);
}

CardLayout::~CardLayout()
{
    qDeleteAll(cards);
    if(moneyLabel != nullptr) delete moneyLabel;
    if(cardLayout != nullptr) delete cardLayout;
    if(layout != nullptr) delete layout;
}

void CardLayout::setCardToUnkown(QLabel *card)
{
    card->setPixmap(CardLayout::blankCard->scaledToHeight(CardLayout::cardHeight, Qt::SmoothTransformation));
}

void CardLayout::setCardsToUnkown()
{
    for(unsigned i = 0; i < cards.size(); i++) {
        setCardToUnkown(cards[i]);
    }
}

void CardLayout::hideCard(QLabel *card)
{
    card->setPixmap(CardLayout::invisCard->scaledToHeight(CardLayout::cardHeight, Qt::SmoothTransformation));
}

void CardLayout::hideCards()
{
    size_t count = cards.size();
    for(size_t i = 0; i < count && cards.size(); i++) {
        hideCard(cards[i]);
    }
}

void CardLayout::setCardsToVisible(unsigned count)
{
    for(unsigned i = 0; i < count && i < cards.size(); i++) {
        if(cards[i]->isVisible() == false) cards[i]->setVisible(true);
    }
}

void CardLayout::changeCard(QLabel *cardLabel, Card card)
{
    // {0, 1, 2, 3} - {"club, spade, heart, diamond"}
    int i = -1, j = -1;

    if(card.getSymbol() == CardSymbol::Clubs) {
        i = 0;
    } else if(card.getSymbol() == CardSymbol::Spades) {
        i = 1;
    } else if(card.getSymbol() == CardSymbol::Hearts) {
        i = 2;
    } else {
        i = 3;
    }

    j = CardNumberToIndex(card.getNumber());
    if(i >= 0 && i < cardImages.size() && j >= 0 && j < cardImages[0].size()) {
        QPixmap cardImage = CardLayout::cardImages[i][j].scaledToHeight(CardLayout::cardHeight, Qt::SmoothTransformation);
        cardLabel->setPixmap(cardImage);
    }
    else {
      //QString s = QString("(i, j) = (%1, %2) | cardImages(1d, 2d) = (%3, %4)");
        qDebug() << "(i, j) = (" << i << ", " << j << ") | cardImages(1d, 2d) = (" << CardLayout::cardImages.size() << ", " << CardLayout::cardImages[0].size() << ")";
      throw std::runtime_error("Slika za kartu nije nadjena");
    }
}

void CardLayout::changeCards(const std::vector<Card> &cards) {
    hideCards();

	unsigned min = qMin((unsigned)cards.size(), cardNumber);

    for(unsigned i = 0; i < min; i++) {
        changeCard(this->cards[i], cards[i]);
    }

    setCardsToVisible(min);
}

QGridLayout *CardLayout::getLayout()
{
    return this->layout;
}

unsigned CardLayout::getMoney()
{
    return money;
}

void CardLayout::setMoney(const unsigned newMoney)
{
    money = newMoney;
    QString text = QString("Money: %1").arg(money);
    moneyLabel->setText(text);
}

void CardLayout::setCardHeight(unsigned newCardHeight)
{
    cardHeight = newCardHeight;
}

unsigned CardLayout::getCardHeight()
{
    return cardHeight;
}

int CardLayout::CardNumberToIndex(CardNumber cn) {
    if(cn == CardNumber::Ace) {
        return 0;
    } else if(cn == CardNumber::Two) {
        return 1;
    } else if(cn == CardNumber::Three) {
        return 2;
    } else if(cn == CardNumber::Four) {
        return 3;
    } else if(cn == CardNumber::Five) {
        return 4;
    } else if(cn == CardNumber::Six) {
        return 5;
    } else if(cn == CardNumber::Seven) {
        return 6;
    } else if(cn == CardNumber::Eight) {
        return 7;
    } else if(cn == CardNumber::Nine) {
        return 8;
    } else if(cn == CardNumber::Ten) {
        return 9;
    } else if(cn == CardNumber::Jack) {
        return 10;
    } else if(cn == CardNumber::Queen) {
        return 11;
    } else if(cn == CardNumber::King) {
        return 12;
    }

    throw std::domain_error("Invalid CardNumber");
}

CardNumber CardLayout::IntToCardNumber(int n) {
    if(n == 2) return CardNumber::Two;
    else if(n == 3) return CardNumber::Three;
    else if(n == 4) return CardNumber::Four;
    else if(n == 5) return CardNumber::Five;
    else if(n == 6) return CardNumber::Six;
    else if(n == 7) return CardNumber::Seven;
    else if(n == 8) return CardNumber::Eight;
    else if(n == 9) return CardNumber::Nine;
    else if(n == 10) return CardNumber::Ten;
    else if(n == 12) return CardNumber::Jack;
    else if(n == 13) return CardNumber::Queen;
    else if(n == 14) return CardNumber::King;
    return CardNumber::Ace;
}
