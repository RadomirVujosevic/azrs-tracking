#ifndef PLAYERLAYOUT_H
#define PLAYERLAYOUT_H

#include <QObject>
#include <QWidget>
#include "ui/layouts/cardlayout.h"

class PlayerLayout : public CardLayout
{
    Q_OBJECT
private:
    const static unsigned cardNumber = 2;
    const bool localPlayer;
    QString name;
    QLabel *nameLabel = nullptr;
    unsigned bet = 0;
    QLabel *betLabel = nullptr;
    int id;

public:
    PlayerLayout(bool localPlayer = false, QString name = QString("Bot"),
                 unsigned money = 10000, QWidget *parent = nullptr);
    ~PlayerLayout();

    bool isLocalPlayer() const;
    const QString &getName() const;
    void setName(const QString &newName);
    int getId() const;
    void setId(int newId);
    void setBet(const unsigned &newBet);
};

#endif // PLAYERLAYOUT_H
