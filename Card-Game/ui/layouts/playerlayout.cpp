#include "playerlayout.h"

PlayerLayout::PlayerLayout(bool localPlayer, QString name,
                           unsigned money, QWidget *parent)
                          : CardLayout{money, cardNumber, parent},
                            localPlayer(localPlayer), name(name)
{
    nameLabel = new QLabel(name, this);

    layout->addWidget(nameLabel, 0, 0, Qt::AlignLeft | Qt::AlignBottom);

    if(betLabel == nullptr) {
        betLabel = new QLabel("Bet: ");
    }
    else betLabel->setText("Bet: ");

    betLabel->setFixedHeight(32);

    setBet(0);

    layout->addWidget(betLabel, 2, 0, Qt::AlignLeft);
    layout->addWidget(moneyLabel, 2, 1, Qt::AlignRight);
}

PlayerLayout::~PlayerLayout()
{
    if(nameLabel != nullptr) delete nameLabel;
    if(betLabel != nullptr) delete betLabel;
}

const QString &PlayerLayout::getName() const
{
    return name;
}

void PlayerLayout::setName(const QString &newName)
{
    name = newName;
    nameLabel->setText(name);
}

int PlayerLayout::getId() const
{
    return id;
}

void PlayerLayout::setId(int newId)
{
    id = newId;
}

void PlayerLayout::setBet(const unsigned &newBet)
{
    bet = newBet;

    if(betLabel != nullptr) {
        betLabel->setText(QString("Bet: %1").arg(newBet));
    }
    else {
        qDebug() << "betLabel is nullptr";
    }
}

bool PlayerLayout::isLocalPlayer() const
{
    return localPlayer;
}
