#ifndef CARDLAYOUT_H
#define CARDLAYOUT_H

#include <QObject>
#include <QWidget>
#include <QString>
#include <QLayout>
#include <QLabel>
#include "client/card.h"

class CardLayout : public QWidget
{
    Q_OBJECT

private:
    static inline QPixmap *blankCard = nullptr;
    static inline QPixmap *invisCard = nullptr;
    static inline std::vector<std::vector<QPixmap>> cardImages;
    static inline unsigned cardHeight = 80;

protected:
    unsigned cardNumber;
    unsigned money;

    QGridLayout *layout = nullptr;
    QLabel *moneyLabel = nullptr;
    std::vector<QLabel*> cards;
    QHBoxLayout *cardLayout = nullptr;

    void setCardToUnkown(QLabel *card);
    void hideCard(QLabel *card);
    void changeCard(QLabel *cardLabel, Card card);

public:
    explicit CardLayout(unsigned money = 10000, unsigned cardNumber = 0,
                        QWidget *parent = nullptr);
    ~CardLayout();

    static void setCardImages(QPixmap *newBlankCard, QPixmap *newInvisCard,
                    const std::vector<std::vector<QPixmap>> &newCardImages);
    static void setCardHeight(unsigned newCardHeight);
    static unsigned getCardHeight();

    static int CardNumberToIndex(CardNumber cn);
    static CardNumber IntToCardNumber(int n);

    void setCardsToUnkown();
    void hideCards();
    void setCardsToVisible(unsigned count);
    void changeCards(const std::vector<Card> &cards);

    QGridLayout* getLayout();
    bool isLocalPlayer() const;
    const QString &getName() const;
    void setName(const QString &newName);
    unsigned getMoney();
    void setMoney(const unsigned newMoney);

signals:

};

#endif // PLAYERLAYOUT_H
