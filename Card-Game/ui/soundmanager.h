#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <QSoundEffect>
#include <QUrl>

class SoundManager
{
public:
    SoundManager(unsigned volume = 100);

    void playPlayerJoined();
    void playPlayerLeft();
    void playGameStarted();
    void playPlayerWon();
    void playShowCards();
    void playCardsChanged();
    void playPlayerCountChanged();

    void setVolume(unsigned newVolume);

private:
    unsigned soundVolume = 100;

    QSoundEffect playerJoined;
    QSoundEffect playerLeft;
    QSoundEffect gameStarted;
    QSoundEffect playerWon;
    QSoundEffect showCards;
    QSoundEffect cardsChanged;
    QSoundEffect playerCountChanged;
};

#endif // SOUNDMANAGER_H
